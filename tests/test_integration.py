async def test_list_news(db, cli, json_data):
    response = await cli.get("/")
    assert response.status == 200
    data = await response.json()
    assert data["news_count"] == 3
    assert len(data["news"]) == data["news_count"]
    assert list(map(lambda r: r["id"], data["news"])) == [3, 5, 1]


async def test_get_news_valid(db, cli, json_data):
    response = await cli.get("/news/1")
    assert response.status == 200
    data = await response.json()
    assert data["comments_count"] == 3
    assert len(data["comments"]) == data["comments_count"]
    assert list(map(lambda r: r["id"], data["comments"])) == [2, 3, 1]


async def test_get_deleted_news(db, cli, json_data):
    response = await cli.get("/news/2")
    assert response.status == 404


async def test_get_future_news(db, cli, json_data):
    response = await cli.get("/news/4")
    assert response.status == 404
