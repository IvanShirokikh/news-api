import pathlib
from json import load as json_load

import pytest

from app.db.main import get_engine
from app.main import init_app
from app.utils import build_config_from_env, upsert_data_from_json

BASE_DIR = pathlib.Path(__file__).parent.parent
TEST_DATA_PATH = BASE_DIR / "tests" / "data"


@pytest.fixture
async def db():
    config = build_config_from_env()
    engine = await get_engine(config["postgres"])
    async with engine.acquire() as conn:
        await conn.execute("DROP DATABASE IF EXISTS test_news;")
        await conn.execute("CREATE DATABASE test_news;")
        yield
        await conn.execute("DROP DATABASE test_news;")
    engine.close()
    await engine.wait_closed()


@pytest.fixture
async def cli(aiohttp_client):
    config = build_config_from_env()
    config["postgres"]["database"] = "test_news"
    app = await init_app(config)
    return await aiohttp_client(app)


@pytest.fixture
async def json_data():
    config = build_config_from_env()
    config["json_data_path"] = TEST_DATA_PATH
    config["postgres"]["database"] = "test_news"
    await upsert_data_from_json(config)
    yield
