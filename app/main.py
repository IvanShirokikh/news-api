import logging
from functools import partial

from aiohttp import web

from app.db.main import close_pg, init_pg, setup_db
from app.middlewares import setup_middlewares
from app.routes import setup_routes
from app.utils import build_config_from_env, upsert_data_from_json


async def init_app(config):
    app = web.Application()
    app["config"] = config
    app.on_startup.append(init_pg)
    app.on_startup.append(setup_db)
    app.on_startup.append(partial(upsert_data_from_json, config))
    app.on_cleanup.append(close_pg)
    setup_routes(app)
    setup_middlewares(app)

    return app


def main():
    config = build_config_from_env()
    app = init_app(config)
    logging.basicConfig(level=getattr(logging, config["log_level"], "INFO"))
    web.run_app(app, host=config["host"], port=config["port"])


if __name__ == "__main__":
    main()
