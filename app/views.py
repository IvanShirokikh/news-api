from functools import partial
from json import dumps as json_dumps

from aiohttp import web

from app.models import News
from app.utils import defaultconverter


async def list_news(request):
    async with request.app["db"].acquire() as conn:
        news_records = await News.list(conn)
        return web.json_response(
            {
                "news": [record.dict(exclude={"comments"}) for record in news_records],
                "news_count": len(news_records),
            },
            dumps=partial(json_dumps, default=defaultconverter),
        )


async def get_news(request):
    async with request.app["db"].acquire() as conn:
        news_id = request.match_info["id"]
        news_record = await News.get_by_id(conn, news_id)
        return web.json_response(
            news_record.dict(),
            dumps=partial(json_dumps, default=defaultconverter),
        )
