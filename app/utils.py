from datetime import datetime
from json import load as json_load
from os import getenv

from app.db.main import get_engine
from app.models import Comment, News


async def upsert_data_from_json(config, app=None):
    try:
        news = json_load(open(f"{config['json_data_path']}/news.json"))
        comments = json_load(open(f"{config['json_data_path']}/comments.json"))
        if app:
            engine = app["db"]
        else:
            engine = await get_engine(config["postgres"])
        async with engine.acquire() as conn:
            for record in news["news"]:
                await News(**record).save(conn)
            for record in comments["comments"]:
                await Comment(**record).save(conn)
        if not app:
            engine.close()
            await engine.wait_closed()
    except FileNotFoundError:
        pass


def defaultconverter(obj):
    if isinstance(obj, datetime):
        return obj.replace(microsecond=0).isoformat()


def build_config_from_env():
    return {
        "postgres": {
            "database": getenv("DATABASE_NAME", "news"),
            "user": getenv("POSTGRES_USER", "postgres"),
            "password": getenv("POSTGRES_PASSWORD", "postgres"),
            "host": getenv("POSTGRES_HOST", "127.0.0.1"),
            "port": getenv("POSTGRES_PORT", "5432"),
            "minsize": int(getenv("DATABASE_MINSIZE", 1)),
            "maxsize": int(getenv("DATABASE_MAXSIZE", 5)),
        },
        "host": getenv("APP_HOST", "127.0.0.1"),
        "port": getenv("APP_PORT", "8080"),
        "log_level": getenv("APP_LOG_LEVEL", "DEBUG"),
        "json_data_path": getenv("JSON_DATA_PATH", "/tmp/news_data"),
    }
