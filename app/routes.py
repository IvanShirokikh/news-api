from app.views import get_news, list_news


def setup_routes(app):
    app.router.add_get("/", list_news)
    app.router.add_get("/news/{id}", get_news)
