from aiohttp import web

from app.models import NotFound


async def handle_404(msg):
    return web.json_response({"error": msg}, status=404)


async def handle_500(msg):
    return web.json_response({"error": msg}, status=500)


def create_error_middleware(overrides):
    @web.middleware
    async def error_middleware(request, handler):
        try:
            return await handler(request)
        except NotFound as e:
            return await overrides[404](str(e))
        except web.HTTPException as e:
            override = overrides.get(e.status)
            if override:
                return await override(str(e))
            raise
        except Exception:
            return await overrides[500]("Internal server error")

    return error_middleware


def setup_middlewares(app):
    error_middleware = create_error_middleware({404: handle_404, 500: handle_500})
    app.middlewares.append(error_middleware)
