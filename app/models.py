from datetime import datetime
from typing import List

from pydantic import BaseModel

from app.db import query


class NotFound(Exception):
    pass


class Comment(BaseModel):
    id: int
    news_id: int
    title: str
    date: datetime
    comment: str

    async def save(self, conn) -> "Comment":
        await query.upsert_comment(conn, self.dict())
        return self


class News(BaseModel):
    id: int
    title: str
    date: datetime
    body: str
    deleted: bool
    comments: List["Comment"] = list()
    comments_count: int = 0

    @classmethod
    async def get_by_id(cls, conn, news_id: int) -> "News":
        record = await query.get_news(conn, news_id)
        if record is None:
            raise NotFound(f"News with id {news_id} don't exist or deleted")
        comments = await query.get_comments_for_news(conn, news_id)
        return cls(**record, comments=comments, comments_count=len(comments))

    @classmethod
    async def list(cls, conn) -> List["News"]:
        return [cls(**record) for record in await query.list_news(conn)]

    async def save(self, conn) -> "News":
        await query.upsert_news(
            conn,
            self.dict(exclude={"comments", "comments_count"}),
        )
        return self
