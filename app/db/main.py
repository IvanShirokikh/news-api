import aiopg.sa
from psycopg2.errors import DuplicateTable
from sqlalchemy.sql.ddl import CreateTable

from app.db.schema import comments_table, news_table


async def get_engine(conf):
    return await aiopg.sa.create_engine(
        database=conf["database"],
        user=conf["user"],
        password=conf["password"],
        host=conf["host"],
        port=conf["port"],
        minsize=conf["minsize"],
        maxsize=conf["maxsize"],
    )


async def init_pg(app):
    conf = app["config"]["postgres"]
    engine = await get_engine(conf)
    app["db"] = engine


async def setup_db(app):
    async with app["db"].acquire() as conn:
        for table in [news_table, comments_table]:
            try:
                await conn.execute(CreateTable(table))
            except DuplicateTable:
                pass


async def close_pg(app):
    app["db"].close()
    await app["db"].wait_closed()
