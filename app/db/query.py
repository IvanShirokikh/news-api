from datetime import datetime

from sqlalchemy import (
    desc as sql_desc,
    func as sql_func,
    outerjoin as sql_outerjoin,
    select as sql_select,
)
from sqlalchemy.dialects.postgresql import insert as sql_insert

from app.db.schema import comments_table, news_table


async def list_news(conn):
    join = sql_outerjoin(
        news_table, comments_table, news_table.c.id == comments_table.c.news_id
    )
    query = (
        sql_select(
            [
                news_table.c.id,
                news_table.c.title,
                news_table.c.date,
                news_table.c.body,
                news_table.c.deleted,
                sql_func.count(comments_table.c.id).label("comments_count"),
            ]
        )
        .select_from(join)
        .where(news_table.c.deleted == False)
        .where(news_table.c.date < datetime.now())
        .order_by(sql_desc(news_table.c.date), news_table.c.id)
        .group_by(
            news_table.c.id,
            news_table.c.title,
            news_table.c.date,
            news_table.c.body,
            news_table.c.deleted,
        )
    )
    result = await conn.execute(query)
    return await result.fetchall()


async def get_news(conn, news_id):
    query = (
        sql_select([news_table])
        .where(news_table.c.id == news_id)
        .where(news_table.c.deleted == False)
        .where(news_table.c.date < datetime.now())
    )
    result = await conn.execute(query)
    return await result.fetchone()


async def get_comments_for_news(conn, news_id):
    query = (
        sql_select([comments_table])
        .where(comments_table.c.news_id == news_id)
        .order_by(sql_desc(comments_table.c.date), comments_table.c.id)
    )
    result = await conn.execute(query)
    return await result.fetchall()


async def upsert_news(conn, data):
    insert = sql_insert(news_table).values(**data)
    return await conn.execute(
        insert.on_conflict_do_update(
            constraint=news_table.primary_key,
            set_=dict(
                title=insert.excluded.title,
                date=insert.excluded.date,
                body=insert.excluded.body,
                deleted=insert.excluded.deleted,
            ),
        )
    )


async def upsert_comment(conn, data):
    insert = sql_insert(comments_table).values(**data)
    return await conn.execute(
        insert.on_conflict_do_update(
            constraint=comments_table.primary_key,
            set_=dict(
                news_id=insert.excluded.news_id,
                title=insert.excluded.title,
                date=insert.excluded.date,
                comment=insert.excluded.comment,
            ),
        )
    )
