from datetime import datetime

from sqlalchemy import MetaData
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Integer,
    ForeignKey,
    String,
    Table,
    Text,
)


metadata = MetaData()
news_table = Table(
    "news",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("title", String, nullable=False),
    Column("date", DateTime, nullable=False, default=datetime.now()),
    Column("body", Text, nullable=False),
    Column("deleted", Boolean, nullable=False, default=True),
)
comments_table = Table(
    "comments",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("news_id", Integer, ForeignKey("news.id", ondelete="CASCADE")),
    Column("title", String, nullable=False),
    Column("date", DateTime, nullable=False, default=datetime.now()),
    Column("comment", Text, nullable=False),
)
